def validateCreditCardNumberFormat(number):
    if not (len(number) == 15 or len(number) == 16):
        return False
    elif number.isnumeric():
        return True
    else:
        return False

def validateCreditCardNumber(number):
    #Luhn algorithm
    number = number[::-1]
    sum = 0
    for i, digit in enumerate(number):
        digit = int(digit)
        if (i % 2 == 0):
            sum += digit
        else:
            digit = digit * 2
            if (digit >= 10):
                digitStr = str(digit)
                sum += int(digitStr[0]) + int(digitStr[1])
            else:
                sum += digit
    return True if sum % 10 == 0 else False


if __name__ == '__main__':
    print ("Please input the credit card number")
    creditCardNumber = str(input())
    if validateCreditCardNumberFormat(creditCardNumber) == False:
        print("Wrong Credit Card Number Format")
    else:
        if (validateCreditCardNumber(creditCardNumber) == True):
            print("Correct Credit Card Number")
        else:
            print("Wrong Credit Card Number")